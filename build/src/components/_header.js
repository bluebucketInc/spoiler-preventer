
// lib imports
import React, { Fragment, useEffect } from "react";
import Spritesheet from "react-responsive-spritesheet";

// sprite instance
let spritesheetInstance = undefined;

// header
const Header = ({ isActive }) => {

	// run if there is an update
	useEffect(() => {
		if (isActive && spritesheetInstance) {
			spritesheetInstance.goToAndPlay(1);
		} else if(!isActive && spritesheetInstance) {
			setTimeout(() => spritesheetInstance.goToAndPlay(1), 200);
		}
	}, [isActive]);

	// render
	return (
		<Fragment>
			<div className={`header-wrapper`}>
				<Spritesheet
					className="sprite_container"
					image={require('../images/strg-sprite.png')}
					widthFrame={604}
					heightFrame={480}
					fps={15}
					steps={72}
					autoplay={true}
					loop={false}
					getInstance={spritesheet => {spritesheetInstance = spritesheet}}
				/>
				<img src={require('../images/road-bg.png')} alt="spoiler preventer" className={`off-background ${isActive ? 'fadeOut': 'fadeIn'}`} />
				<img src={require('../images/preventer-sign-off.png')} alt="spoiler preventer" className={`preventer off ${isActive ? 'fadeOut': 'fadeIn'}`} />
				<img src={require('../images/preventer-sign-on.png')} alt="spoiler preventer" className={`preventer ${isActive ? 'fadeIn': 'fadeOut'}`} />
			</div>
			<div className="content-wrapper">
				<h2>Prevent Accidental Spoilers.<br/>Thanks to Collision Prevention Assist.</h2>
				<a href="https://www.mercedes-benz.com.sg" target="_blank" rel="noopener noreferrer">Find out more</a>
			</div>
			<div className="logo-wrapper">
				<img src={require('../images/mercedes-logo-txt.png')} alt="Mercedes Logo" className="logo-txt" />
				<img src={require('../images/mercedes-logo.png')} alt="Mercedes Logo" className="logo" />
			</div>
		</Fragment>
	);
};

// export
export default Header;