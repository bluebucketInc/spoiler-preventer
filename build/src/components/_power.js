
// lib imports
import React from "react";

// power button
const Power = ({ onPowerToggle, isActive }) => (
	<div className="checkbox-wrapper">
		<div className="checkbox-container">
			<input type="checkbox" className="switch" checked={isActive} onChange={onPowerToggle} />
			<span className="toggle-text">{ isActive ? 'ON' : 'OFF' }</span>
		</div>
	</div>
);

// export
export default Power;
