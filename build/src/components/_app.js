// lib imports
import React, { useEffect } from "react";
import useThunkReducer from "react-hook-thunk-reducer";

// components
import Header from "./_header";
import Power from "./_power";

// state
import { initialState, reducer } from "../state/_reducer";
// actions
import {
	handlePowerToggle,
	getStoredState,
} from "../state/_actions";

// main app
const App = () => {

	// hook
	const [state, dispatch] = useThunkReducer(reducer, initialState);

	// fetch initial state from storage
	useEffect(() => dispatch(getStoredState()), []);

	// check for updates

	// on new update

	// render
	return (
		<div className="pageContainer">
			<Header isActive={state.isActive} />
			<Power
				onPowerToggle={e => dispatch(handlePowerToggle(e))}
				isActive={state.isActive}
			/>
		</div>
	);
};

// export
export default App;