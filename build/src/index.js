
// imports
import React from 'react';
import ReactDOM from 'react-dom';

// styles
import 'normalize.css';
import "./styles/main.scss";

// app
import App from './components/_app.js';

// render
ReactDOM.render(<App />, document.getElementById('root'));