/* global chrome */
/* global ga */

// imports
import { sendActivationMessage, fetchStoredState, storeState } from "../utils/_helpers";

// power toggle
export const handlePowerToggle = e => dispatch => {
	// send analytics
	ga('send', 'event', 'Power-Button', e.target.checked ? 'ON' : 'OFF');
	// send activation message to current tab
	sendActivationMessage(e.target.checked);
	// store state
	storeState({isActive: e.target.checked});
	// state
	return dispatch({
		type: "set-active",
		payload: e.target.checked
	});
};

// get stored state
export const getStoredState = () => dispatch => {
	fetchStoredState.then(state =>
		dispatch({
			type: "set-stored-state",
			payload: state
		})
	);
}