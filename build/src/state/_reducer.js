
// imports
import { strangerthings_dict } from '../utils/_keywords';

// initial state
export const initialState = {
	isActive: true,
	availableShows: [
		{
			id: 'strg',
			name: 'Stranger Things',
			keywords: strangerthings_dict,
		},
	],
	activeShows: [
		{
			id: 'strg',
			isActive: true,
		}
	],
	activeKeywords: [...strangerthings_dict],
	version: '0.1',
};

// reducer
export const reducer = (state, action) => {
	
	switch (action.type) {
		case "set-active":
			return {
				...state,
				isActive: action.payload,
			};
		case "set-stored-state":
			return {
				...state,
				...action.payload,
			};
		default:
			throw new Error();
	}
};