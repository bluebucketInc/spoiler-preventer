
// stranger things keywords
export const strangerthings_dict = [
	"strangerthings",
	"stranger things",
	"millie bobby brown",
	"finn wolfhard",
	"sadies ink",
	"eleven",
	"mileven",
	"noah schnapp",
	"gaten matarazzo",
	"mike wheeler",
	"finn",
	"will byers",
	"upside down",
	"demogorgon",
	"smoke monster"
];
