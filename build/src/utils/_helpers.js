/* global chrome */

// send activation to current tab
export const sendActivationMessage = state =>
	chrome.tabs.query({active: true, currentWindow: true}, tabs =>
		chrome.tabs.sendMessage(tabs[0].id, {
			name: "SpoilerPreventer",
			message: "setActiveState",
			data: { isActive: state },
		})
	);

// fetch data from chrome
export const fetchStoredState =
	new Promise((resolve, reject) =>
		chrome.storage.local.get(
			["SpoilerPreventerState"], result => resolve(result.SpoilerPreventerState)));

// save local storage data
export const storeState = newState =>
	fetchStoredState.then(oldState => 
		chrome.storage.local.set({
			SpoilerPreventerState: {
				...oldState,
				...newState,
			}
		})
	);