
// vars
let mutationConfig = { subtree: true, attributes: true };
let spoilerState = {};

// Replace Function
// looks through content dom and look for matches
// adds spoiler class into the dom node
const insertSpoilerClass = ( dictionary, show ) => {
	// select spoiler containing elements
	let tags = $('*').not('.spoiler-blocked');
	let images = $('img').not('.blocker-container_img');

	// try catch container
	try {
		// loop through all tags
		for (let i = 0; i < tags.length; i++) {
			let element = tags[i];
			for ( let j = 0; j < element.childNodes.length; j++) {
				let node = element.childNodes[j];
				// TEXT
				if (node.nodeType === 3) {
					let text = node.nodeValue.toLowerCase();
					// text = text;
					dictionary && dictionary.forEach( dictionaryItem => {
						if (text.includes(dictionaryItem) && !$(element).hasClass("spoiler-blocked")) {
							$(element).addClass('blocker-container_txt');
						}
					});
				}
			}
		}
		// loop through all images
		for (let img of images) {
			dictionary && dictionary.forEach( dictionaryItem => {
				if (img.alt.includes(dictionaryItem) && !$(img.parentNode).hasClass("spoiler-blocked")) {
					$(img.parentNode).addClass("spoiler-blocked");
					// $(img.parentNode).attr('data-keyword', dictionaryItem);
					img.parentNode.innerHTML += `<div class="blocker-container_img ${show}-img"></div>`;
				}
			});
		}
	} catch(e) { console.warn(e) };
};

// get current state from storage
const getCurrentState = () =>
	new Promise( resolve =>
		chrome.storage.local.get(['SpoilerPreventerState'], result => {
			spoilerState = result.SpoilerPreventerState;
			resolve(result.SpoilerPreventerState);
		})
	);

// set active or inavtive state globally
const setSpoilerClass = state => {
	const body = $(document.body);
	state ? body.addClass('spoiler-blocker-active') : body.removeClass('spoiler-blocker-active');
};

// initial run after DOM content is loaded
// check state and update class if needed
const init = () =>
	getCurrentState().then(response => {
		setSpoilerClass(spoilerState.isActive);
		insertSpoilerClass(spoilerState.activeKeywords, 'stranger things');
	});

// on page load
document.addEventListener("DOMContentLoaded", () => {

	// set location
	urlState = window.location.href;
 
	// initate
	init();

	// mutation observer for changes
	const observer = new MutationObserver( () => {
		
		// mutate spoiler objects
		insertSpoilerClass(spoilerState.activeKeywords, 'stranger things');
		// set active global state
		setSpoilerClass(spoilerState.isActive);

		// update state if facebook
		if(urlState.includes('facebook') && urlState != window.location.href) {
			urlState = window.location.href;
		}

	});

	// Google image search
	if(document.getElementById('search')) {
		observer.observe(document.getElementById('search'), mutationConfig);
	}
	
	// document observer for facebook
	if(urlState.includes('facebook') || window.location.hostname.includes('reddit') || window.location.href.includes('www.youtube.com/results')) {
		observer.observe(document, mutationConfig);
	}

}, false);


// Messaging Board //
chrome.runtime.onMessage.addListener( function(request, sender, sendResponse) {
	if(request.name == 'SpoilerPreventer')
		// for each message types
		switch (request.message) {
			case "tabChanged":
				return getCurrentState().then(response => setSpoilerClass(response.isActive));
			case "setActiveState":
				return getCurrentState().then(response => setSpoilerClass(response.isActive));
			case "stateUpdated":
				return getCurrentState();
			default:
		}
});





