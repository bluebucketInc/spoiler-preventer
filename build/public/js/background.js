

// initial keywords of the app during install
const strangerthings_dict = [
	"strangerthings",
	"stranger things",
	"millie bobby brown",
	"finn wolfhard",
	"sadies ink",
	"eleven",
	"mileven",
	"noah schnapp",
	"gaten matarazzo",
	"mike wheeler",
	"finn",
	"will byers",
	"upside down",
	"demogorgon",
	"smoke monster",
	"mind flayer",
	"smoke monster",
	"stranger_things",
	"stranger-things"
];

// every time user switches between tabs
// tell the content script to check the latest state
chrome.tabs.onActivated.addListener( activeInfo =>
	chrome.tabs.sendMessage(activeInfo.tabId, { name: "SpoilerPreventer", message: "tabChanged" }));

// set the storaed state during the first install
// this will be replaced after popup gets the latest vesion
chrome.runtime.onInstalled.addListener(function(details){
    if(details.reason == "install"){
		// set default state
		chrome.storage.local.set({
			SpoilerPreventerState: {
				isActive: true,
				availableShows: [
					{
						id: 'strg',
						name: 'Stranger Things',
						keywords: strangerthings_dict,
					},
				],
				activeShows: [
					{
						id: 'strg',
						isActive: true,
					}
				],
				activeKeywords: [...strangerthings_dict],
				version: '0.1',
			}
		});
    }
});